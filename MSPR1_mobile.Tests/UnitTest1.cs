using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using MSPR1_mobile.Models;
using MSPR1_mobile.Services;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace MSPR1_mobile.Tests
{
    public class Tests
    {
        WebService _webService;

        public Tests()
        {
            _webService = new WebService();
        }

        // GET /api/
        [Test]
        public void TestSimpleApi()
        {
            Result result = _webService.GetDataAsync<Result>(string.Format("{0}", Constants.UrlApi)).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "1");
        }

        // POST /api/ email - password
        [Test]
        public void LoginFailAccountDoesNotExist()
        {
            string email = "";
            string password = Utility.CreateMD5Hash("");
            string data = string.Format("emailLogin={0}&passwordLogin={1}", email, password);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "1");
        }

        // POST /api/ email - password
        [Test]
        public void LoginFailPasswordDoesNotMatch()
        {
            string email = "test@mspr.fr";
            string password = Utility.CreateMD5Hash("");
            string data = string.Format("emailLogin={0}&passwordLogin={1}", email, password);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "1");
        }

        // POST /api/ email - password
        [Test]
        public void LoginSuccess()
        {
            string email = "test@mspr.fr";
            string password = Utility.CreateMD5Hash("test");
            string data = string.Format("emailLogin={0}&passwordLogin={1}", email, password);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "0");
            Assert.IsNotNull(result.Account);
            Assert.AreEqual(result.Account.Id, "5e819d274a5d6");
        }

        // POST /api/ email - password - name - firstname
        [Test]
        public void RegisterFailEmailAlreadyExist()
        {
            string email = "test@mspr.fr";
            string password = Utility.CreateMD5Hash("test");
            string name = "MSPR";
            string firstname = "Test";
            string data = string.Format("email={0}&password={1}&name={2}&firstname={3}", email, password, name, firstname);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "1");
        }

        // POST /api/ email - password - name - firstname
        [Test]
        public void RegisterSuccess()
        {
            string email = "test2@mspr.fr";
            string password = Utility.CreateMD5Hash("test");
            string name = "MSPR";
            string firstname = "Test";
            string data = string.Format("email={0}&password={1}&name={2}&firstname={3}", email, password, name, firstname);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "0");
            Assert.IsNotNull(result.Account);
            Assert.AreEqual(result.Account.Email, "test2@mspr.fr");
            // delete account now
            HttpContent content2 = new StringContent("emailDelete=" + email, Encoding.UTF8, "application/x-www-form-urlencoded");
            Result result2 = _webService.PostDataAsync<Result>(content2).Result;
        }

        // POST /api/ id - email - name - firstname
        [Test]
        public void UpdateAccountFailIdNotExist()
        {
            string email = "test@mspr.fr";
            string id = "test";
            string name = "MSPR";
            string firstname = "Test";
            string data = string.Format("email={0}&id={1}&name={2}&firstname={3}", email, id, name, firstname);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "1");
        }

        // POST /api/ id - email - name - firstname
        [Test]
        public void UpdateAccountSuccess()
        {
            string email = "test@mspr.fr";
            string id = "5e819d274a5d6";
            string name = "MSPR";
            string firstname = "Test";
            string data = string.Format("email={0}&id={1}&name={2}&firstname={3}", email, id, name, firstname);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = _webService.PostDataAsync<LoginResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "0");
            Assert.IsNotNull(result.Account);
            Assert.AreEqual(result.Account.Email, email);
            Assert.AreEqual(result.Account.Name, name);
            Assert.AreEqual(result.Account.Firstname, firstname);
        }

        // GET - account id
        [Test]
        public void ListCoupon()
        {
            CouponResult result = _webService.GetDataAsync<CouponResult>(string.Format("{0}?token={1}", Constants.UrlApi, "5e819d274a5d6")).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "0");
            Assert.IsInstanceOf<List<Coupon>>(result.Coupons);
        }

        // GET - qr code id / account id
        [Test]
        public void AddCouponFailQrCodeNotExist()
        {
            string data = string.Format("qrcode={0}&token={1}", "unknown", "5e819d274a5d6");
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            CouponResult result = _webService.PostDataAsync<CouponResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "1");
        }

        // GET - qr code id / account id
        [Test]
        public void AddCouponSuccess()
        {
            string data = string.Format("qrcode={0}&token={1}", "5e1ed1c797a95", "5e819d274a5d6");
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            CouponResult result = _webService.PostDataAsync<CouponResult>(content).Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Status, "0");
        }


    }
}