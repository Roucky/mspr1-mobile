﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Essentials;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency(typeof(MSPR1_mobile.Droid.ZXingHelper))]
namespace MSPR1_mobile.Droid
{
    public class ZXingHelper : IZXingHelper
    {
        public CameraResolution SelectLowestResolutionMatchingDisplayAspectRatio(List<CameraResolution> availableResolutions)
        {
            CameraResolution result = availableResolutions[0];
            //a tolerance of 0.1 should not be recognizable for users
            double aspectTolerance = 0.1;
            //calculating our targetRatio
            var targetRatio = DeviceDisplay.MainDisplayInfo.Height / DeviceDisplay.MainDisplayInfo.Width;
            var targetHeight = DeviceDisplay.MainDisplayInfo.Height;
            var minDiff = double.MaxValue;
            //camera API lists all available resolutions from highest to lowest, perfect for us
            //making use of this sorting, following code runs some comparisons to select the lowest resolution that matches the screen aspect ratio
            //selecting the lowest makes QR detection actual faster most of the time
            foreach (var r in availableResolutions)
            {
                //if current ratio is bigger than our tolerance, move on
                //camera resolution is provided landscape ...
                if (Math.Abs(((double)r.Width / r.Height) - targetRatio) > aspectTolerance)
                    continue;
                else
                    if (Math.Abs(r.Height - targetHeight) < minDiff)
                    minDiff = Math.Abs(r.Height - targetHeight);
                result = r;
            }
            return result;
        }

    }
}