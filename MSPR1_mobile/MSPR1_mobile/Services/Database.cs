﻿using MSPR1_mobile.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MSPR1_mobile.Services
{
    public class Database
    {
        readonly SQLiteAsyncConnection _database;

        public Database(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Account>().Wait();
        }

        public Task<Account> GetAccount()
        {
            return _database.Table<Account>().FirstOrDefaultAsync() ?? null;
        }
        public Task<int> Disconnect()
        {
            return _database.DeleteAllAsync<Account>();
        }

        public Task<int> SaveAccount(Account account)
        {
            return _database.InsertAsync(account);
        }

    }
}
