﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MSPR1_mobile.Services
{
    public class WebService
    {

        HttpClient _client;

        public WebService()
        {
            _client = new HttpClient();
        }

        public async Task<T> GetDataAsync<T>(string uri = Constants.UrlApi)
        {
            ShowRequest(string.Format("GET - {0}", uri));
            T result = default(T);
            try
            {
                HttpResponseMessage response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public async Task<T> PostDataAsync<T>(HttpContent httpContent, string uri = Constants.UrlApi)
        {
            ShowRequest(string.Format("POST - {0}", uri), httpContent);
            T result = default(T);
            try
            {
                HttpResponseMessage response = await _client.PostAsync(uri, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        private static void ShowRequest(string text, HttpContent httpContent = null)
        {
            Console.WriteLine(string.Format("\n\n API: {0} {1} \n\n", text, (httpContent!=null)?httpContent.ReadAsStringAsync().Result:string.Empty));
        }

    }
}
