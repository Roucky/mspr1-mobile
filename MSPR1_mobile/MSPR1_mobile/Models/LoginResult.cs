﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MSPR1_mobile.Models
{
    public class LoginResult : Result
    {
        [JsonProperty("account")]
        public Account Account { get; set; }
    }
}
