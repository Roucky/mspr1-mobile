﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MSPR1_mobile.Models
{
    public class Coupon
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Libelle")]
        public string Libelle { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Expiration")]
        public string Expiration { get; set; }

        [JsonProperty("ColorCode")]
        public string ColorCode { get; set; }

        [JsonProperty("Deleted")]
        public string Deleted { get; set; }

    }
}
