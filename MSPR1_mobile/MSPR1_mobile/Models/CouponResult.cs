﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MSPR1_mobile.Models
{
    public class CouponResult : Result
    {
        [JsonProperty("coupons")]
        public List<Coupon> Coupons { get; set; }
    }
}
