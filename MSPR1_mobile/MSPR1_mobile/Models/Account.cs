﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MSPR1_mobile.Models
{
    public class Account
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Firstname")]
        public string Firstname { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }
    }
}
