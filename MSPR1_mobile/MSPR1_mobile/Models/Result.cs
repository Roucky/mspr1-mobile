﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MSPR1_mobile.Models
{
    public class Result
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
