﻿using System;
using System.Collections.Generic;
using System.Text;
using ZXing.Mobile;

namespace MSPR1_mobile
{
    public interface IZXingHelper
    {
        CameraResolution SelectLowestResolutionMatchingDisplayAspectRatio(List<CameraResolution> availableResolutions);
    }
}
