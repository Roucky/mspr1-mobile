﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MSPR1_mobile.Models;
using MSPR1_mobile.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MSPR1_mobile.Views.CarouselViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListCouponPage : ContentPage
    {
        WebService _webservice;
        public ListCouponPage()
        {
            _webservice = new WebService();
            InitializeComponent();
        }

        private ICommand Refresh()
        {
            return new Command(async () =>
            {
                CouponListView.ItemsSource = await GetCoupons();
                CouponListView.IsRefreshing = false;
            });
        }

        private async Task<List<Coupon>> GetCoupons()
        {
            Account account = await App.Database.GetAccount();
            CouponResult result = await _webservice.GetDataAsync<CouponResult>(string.Format("{0}?token={1}", Constants.UrlApi, account?.Id));
            if(result?.Coupons == null || result?.Coupons?.Count() == 0)
            {
                NoCoupon.IsVisible = true;
                return null;
            }
            NoCoupon.IsVisible = false;
            return result.Coupons;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            CouponListView.ItemsSource = await GetCoupons();
            CouponListView.RefreshCommand = Refresh();
        }
    }
}