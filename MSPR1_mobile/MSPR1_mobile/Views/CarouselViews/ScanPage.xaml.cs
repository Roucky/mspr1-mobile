﻿using MSPR1_mobile.Models;
using MSPR1_mobile.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;
using MSPR1_mobile.Views.CarouselViews;
using Xamarin.Essentials;

namespace MSPR1_mobile.Views.CarouselViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanPage : ContentPage
    {
        ZXingScannerView zxing;
        ZXingDefaultOverlay overlay;

        WebService _webService;

        public ScanPage()
        {
            InitializeComponent();
            _webService = new WebService();
            zxing = new ZXingScannerView
            {
                IsScanning = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                AutomationId = "zxingScannerView"
            };
            var options = new ZXing.Mobile.MobileBarcodeScanningOptions()
            {
                PossibleFormats = new List<ZXing.BarcodeFormat>() { ZXing.BarcodeFormat.QR_CODE },
                CameraResolutionSelector = DependencyService.Get<IZXingHelper>().SelectLowestResolutionMatchingDisplayAspectRatio,
                DelayBetweenAnalyzingFrames = 5,
                DelayBetweenContinuousScans = 5
            };
            zxing.Options = options;
            zxing.OnScanResult += (result) =>
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await ScanResult(result?.Text);
                });

            overlay = new ZXingDefaultOverlay
            {
                TopText = "Scannez le QR Code",
                ShowFlashButton = zxing.HasTorch,
                AutomationId = "zxingDefaultOverlay"
            };

            var grid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            grid.Children.Add(zxing);
            grid.Children.Add(overlay);

            Content = grid;

        }

        public async Task<bool> ScanResult(string text)
        {
            Vibration.Vibrate(TimeSpan.FromMilliseconds(100));
            zxing.IsAnalyzing = false;
            Account account = await App.Database.GetAccount();
            string data = string.Format("token={0}&qrcode={1}", account?.Id, text);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            CouponResult result = await _webService.PostDataAsync<CouponResult>(content);
            zxing.IsAnalyzing = true;
            if (result?.Status == "0")
            {
                var parent = this.Parent as TabbedPage;
                parent.CurrentPage = parent.Children[1];
                return true;
            }
            return false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }


        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return false;
        }
    }
}