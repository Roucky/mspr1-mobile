﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MSPR1_mobile.Models;
using MSPR1_mobile.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MSPR1_mobile.Views.CarouselViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfilePage : ContentPage
    {
        WebService _webService;
        public EditProfilePage()
        {
            _webService = new WebService();
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            this.BindingContext = await App.Database.GetAccount();
        }

        async void Update(Object sender, EventArgs e)
        {
            Account account = await App.Database.GetAccount();
            string data = string.Format("id={0}&name={1}&firstname={2}&email={3}", account?.Id ?? string.Empty, Name?.Text ?? string.Empty, Firstname?.Text ?? string.Empty, Email?.Text ?? string.Empty);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = await _webService.PostDataAsync<LoginResult>(content, Constants.UrlApi);
            if(result.Status == "0")
            {
                await App.Database.Disconnect();
                await App.Database.SaveAccount(result.Account);
                await DisplayAlert("Résultat", "Votre compte a bien été modifié.", "Ok");
            }
            else
            {
                await DisplayAlert("Résultat", "Une erreur est survenue, veuillez réésayer.", "Ok");
            }
        }
    }
}