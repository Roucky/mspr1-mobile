﻿using MSPR1_mobile.Models;
using MSPR1_mobile.Services;
using MSPR1_mobile.Views.CarouselViews;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MSPR1_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {

        WebService _webService;
        public LoginPage()
        {
            InitializeComponent();
            _webService = new WebService();
            GestureRecognizer tap = new TapGestureRecognizer
            {
                Command = new Command(Register)
            };
            RegisterButton.GestureRecognizers.Add(tap);
        }

        async void Login(object sender, EventArgs e)
        {
            LoginButton.IsEnabled = false;

            string data = string.Format("emailLogin={0}&passwordLogin={1}", Email?.Text ?? string.Empty, Utility.CreateMD5Hash(Password?.Text ?? string.Empty));
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = await _webService.PostDataAsync<LoginResult>(content);
            if (result?.Status == "0" && !string.IsNullOrEmpty(result?.Account?.Id))
            {
                // ADD TO SQLITE DB
                if (!string.IsNullOrEmpty(result?.Account?.Id))
                {
                    await App.Database.SaveAccount(result.Account);
                    NextPage(result.Account);
                }
            }
            LoginButton.IsEnabled = true;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            Account account = await App.Database.GetAccount();
            if (!string.IsNullOrEmpty(account?.Id))
            {
                NextPage(account);
            }
        }

        private async void NextPage(Account account)
        {
            if (await VerifyAccount(account))
            {
                Navigation.PushAsync(new AccountPage());
            }
        }

        private async Task<bool> VerifyAccount(Account account)
        {
            CouponResult result = await _webService.GetDataAsync<CouponResult>(string.Format("{0}?token={1}", Constants.UrlApi, account?.Id));
            return result?.Status == "1";
        }

        void Register()
        {
            Navigation.PushAsync(new RegisterPage());
        }
    }
}