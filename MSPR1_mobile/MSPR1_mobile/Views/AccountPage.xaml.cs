﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSPR1_mobile.Views.CarouselViews;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MSPR1_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountPage : TabbedPage
    {
        protected bool refresh = false;
        public AccountPage()
        {
            InitializeComponent();
            BarBackgroundColor = Color.Black;
            SelectedTabColor = Color.White;
            UnselectedTabColor = Color.FromHex("#666666");
            Children.Add(new ScanPage());
            Children.Add(new ListCouponPage());
            Children.Add(new EditProfilePage());
            CurrentPage = Children[1];
            Title = CurrentPage.Title;
            this.CurrentPageChanged += CurrentPageHasChanged;
        }

        private void CurrentPageHasChanged(object sender, EventArgs e)
        {
            this.Title = this.CurrentPage.Title;
        }


        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }

        async void Logout(object sender, EventArgs e)
        {
            App.Database.Disconnect();
            await Navigation.PopToRootAsync();
        }
    }
}