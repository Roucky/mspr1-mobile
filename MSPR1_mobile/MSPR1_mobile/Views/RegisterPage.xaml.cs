﻿using MSPR1_mobile.Models;
using MSPR1_mobile.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MSPR1_mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        WebService _webService;
        public RegisterPage()
        {
            InitializeComponent();
            _webService = new WebService();
            GestureRecognizer tap = new TapGestureRecognizer
            {
                Command = new Command(Login)
            };
            LoginButton.GestureRecognizers.Add(tap);
        }

        async void Register(object sender, EventArgs e)
        {
            RegisterButton.IsEnabled = false;

            string data = string.Format("email={0}&password={1}&name={2}&firstname={3}", Email?.Text ?? string.Empty, Utility.CreateMD5Hash(Password?.Text ?? string.Empty), Name?.Text ?? string.Empty, Firstname?.Text ?? string.Empty);
            HttpContent content = new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
            LoginResult result = await _webService.PostDataAsync<LoginResult>(content);
            if (result?.Status == "0")
            {
                // ADD TO SQLITE DB
                await App.Database.SaveAccount(result?.Account);
                Navigation.PushAsync(new AccountPage());
            }
            else
            {
                RegisterButton.IsEnabled = true;
            }
        }

        async void Login()
        {
            await Navigation.PopToRootAsync();
        }
    }
}